//
//  ViewController.swift
//  helloSingleView
//
//  Created by Debora Johansson on 10/22/19.
//  Copyright © 2019 Jonatan Larsson. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func buttonPress(_ sender: Any) {
        print("hello button")
    }
}

